"strict";
let firstNum = Number(prompt("Set the first number"));
let secNum = Number(prompt("Set the second number"));
let operator = prompt("Choose any math operator");

function numCalculation(firstNum, secNum, operator) {
  let result;
  if (operator === "+") {
    result = firstNum + secNum;
  } else if (operator === "-") {
    result = firstNum - secNum;
  } else if (operator === "*") {
    result = firstNum * secNum;
  } else if (operator === "/") {
    result = firstNum / secNum;
  }
  return result;
}

alert(numCalculation(firstNum, secNum, operator));
